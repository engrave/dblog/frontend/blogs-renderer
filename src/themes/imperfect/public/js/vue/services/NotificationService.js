import 'izitoast/dist/css/iziToast.min.css'
import iZtoast from 'izitoast'

class NotificationService {

    static error (message, title = 'Error') {
        return iZtoast.error({
            title: title,
            message: message,
            position: 'bottomLeft'
        });
    }

    static success (message, title = 'Success') {
        return iZtoast.success({
            title: title,
            message: message,
            position: 'bottomLeft'
        });
    }

};

export default NotificationService;
