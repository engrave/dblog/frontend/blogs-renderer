import Axios from '../store/axios';

class CommentsService {
    static sendComment(parent_author, parent_permlink, parent_title, body, token) {
        const options = {
            method: 'POST',
            url: '/action/comment',
            headers: {
                'authorization': token
            },
            data: {
                parent_author,
                parent_permlink,
                parent_title,
                body
            }
        };
        return Axios.request(options)
    }

    static editComment(parent_author, parent_permlink, author, permlink, title, body, token) {
        const options = {
            method: 'PUT',
            url: '/action/comment',
            headers: {
                'authorization': token
            },
            data: {
                parent_author,
                parent_permlink,
                author,
                permlink,
                title,
                body
            }
        };
        return Axios.request(options)
    }

    static getComments(author, permlink) {
        return Axios.post('/comments', {author, permlink});
    }

    static getVotes(author, permlink) {
        return Axios.post('https://api.openhive.network', {
            jsonrpc: "2.0",
            method: "condenser_api.get_active_votes",
            params: [`${author}`, `${permlink}`],
            id: 1
        }).then(response => {
            const {data} = response;
            return Promise.resolve(data.result);
        });
    }
}

export default CommentsService;
