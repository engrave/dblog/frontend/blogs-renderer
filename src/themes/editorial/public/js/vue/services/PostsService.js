import Axios from '../store/axios';

export default class PostsService {

    static loadMore(blogId, categoryId, skip) {
        let options = {
            method: 'GET',
            headers: {
                'blogId': blogId,
                'skip': skip
            },
            url: '/posts',
        };

        if(categoryId) {
           options.headers.categoryId = categoryId;
        }

        return Axios(options);
    }
}
