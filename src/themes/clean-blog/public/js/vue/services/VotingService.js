import Axios from '../store/axios';

class VotingService {
    static vote(author, permlink, weight, token) {
        const options = {
            method: 'POST',
            url: '/action/vote',
            headers: {
                'authorization': token
            },
            data: {
                author,
                permlink,
                weight
            }
        };
        return Axios.request(options)
    }

    static getVotes (author, permlink) {
        return Axios.post('https://api.openhive.network', {
            jsonrpc: "2.0",
            method: "condenser_api.get_active_votes",
            params: [`${author}`, `${permlink}`],
            id: 1
        }).then(response => {
            const {data} = response;
            return Promise.resolve(data.result);
        });
    }

    static getValue (author, permlink) {
        return Axios.post('https://api.openhive.network', {
            jsonrpc: "2.0",
            method: "condenser_api.get_content",
            params: [`${author}`, `${permlink}`],
            id: 1
        }).then(response => {
            const {data} = response;
            const v1 = data.result.pending_payout_value ? data.result.pending_payout_value.replace(" SBD", "").replace(" HBD", "") : ''
            const v2 = data.result.total_payout_value ? data.result.total_payout_value.replace(" SBD", "").replace(" HBD", "") : ''
            const v3 = data.result.curator_payout_value ? data.result.curator_payout_value.replace(" SBD", "").replace(" HBD", "") : ''
            return Promise.resolve(parseFloat(v1) + parseFloat(v2) + parseFloat(v3));
        });
    }

}

export default VotingService;
