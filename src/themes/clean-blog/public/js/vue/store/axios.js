import Axios from 'axios';
import NotificationService from "../services/NotificationService";

const errorResponseHandler = (error) => {
    // check for errorHandle config
    if (error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false) {
        return Promise.reject(error);
    }

    // if has response show the error
    if (error.response) {
        console.error(error.response)
        if(error.response.data.message) {
            NotificationService.error(error.response.data.message)
        } else {
            NotificationService.error('Unexpected error occured');
        }
    }
}

Axios.interceptors.response.use(
    response => response,
    errorResponseHandler
);

export default Axios;
