import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        sso: {
            loggedIn: false,
            token: null,
            username: null
        }
    },
    mutations: {
        ssoLogin(state, payload) {
            state.sso.loggedIn = true;
            state.sso.username = payload.username;
            state.sso.token = payload.token;
        },
        ssoLogout(state) {
            state.sso.loggedIn = false;
            state.sso.username = null;
            state.sso.token = null;
        }
    }
});
