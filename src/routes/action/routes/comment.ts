import { Request, Response } from 'express';
import { handleResponseError, logger } from '../../../submodules/shared-library';
import { body, header } from 'express-validator/check';
import auth from '../../../services/auth/auth';
import sc from '../../../submodules/shared-library/services/steemconnect/steemconnect.service';
import vault from '../../../services/vault/vault.service';
import renderSteemCommentBody from '../../../submodules/shared-library/services/article/renderSteemCommentBody';

const middleware: any[] =  [
    body('parent_author').isString().not().isEmpty(),
    body('parent_permlink').isString().not().isEmpty(),
    body('parent_title').isString(),
    body('body').isString().not().isEmpty(),

    header('authorization').isString().not().isEmpty()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { authorization: token } = req.headers;
        const { parent_author, parent_permlink, parent_title, body } = req.body;

        const {valid, payload: {data: { username }}} = await auth.validateJwt(token);

        if(!valid) throw new Error("JWT not valid")

        const access_token = await vault.getAccessToken(username);

        const { result } = await sc.comment(access_token, username, `RE: ${parent_title}`, body, parent_author, parent_permlink);

        logger.info(`Comment added successfully by: ${username} at ${parent_author}, ${parent_permlink}`);

        return res.json({
            success: 'Comment added successfully',
            body,
            title: `RE: ${parent_title}`,
            rendered: await renderSteemCommentBody(body),
            author: username,
            permlink: result.operations [0][1].permlink, // TODO cannot get 0 from undefined
            result
        });

    }, req, res);
}

export default {
    middleware,
    handler
}
