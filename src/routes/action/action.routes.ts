import * as express from "express";
import addComment from "./routes/comment";
import editComment from "./routes/editComment";
import vote from "./routes/vote";

const actionApi: express.Router = express.Router();

actionApi.post('/comment', addComment.middleware, addComment.handler);
actionApi.put('/comment', editComment.middleware, editComment.handler);
actionApi.post('/vote', vote.middleware, vote.handler);

export default actionApi;
