import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { header } from 'express-validator/check';
import { getLatestFromCategory, getLatestArticles } from '../../../submodules/shared-library/services/cache/cache';

const middleware: any[] =  [
    header('blogId').isMongoId(),
    header('categoryId').optional().isString(),
    header('skip').optional().isNumeric()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        
        const { blogid, skip, categoryid } = req.headers;
        
        let posts:any = [];

        if(categoryid) {
            posts = await getLatestFromCategory(<string>categoryid, <string>blogid, skip ? parseInt(<string>skip) : 0, 12);
        } else {
            posts = await getLatestArticles(<string>blogid, skip ? parseInt(<string>skip) : 0, 12);
        }
        
        return res.json({ posts });

    }, req, res);
}

export default {
    middleware,
    handler
}