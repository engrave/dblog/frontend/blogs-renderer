import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { getBlog } from '../../../submodules/shared-library/services/cache/cache';
import sitemap from '../../../services/sitemap/sitemap.service';

const middleware: any[] =  [];

async function handler(req: Request, res: Response) {

    return handleResponseError(async () => {

        const { hostname } = req;

        const blog = await getBlog(hostname);
        
        if(blog.domain_redirect && blog.custom_domain && hostname != blog.custom_domain) {
            return res.redirect(`https://${blog.custom_domain}/sitemap.xml`);
        }

        const xml = await sitemap.getSitemap(hostname);

        return res.header('Content-Type', 'application/xml').send(xml);
        
    }, req, res);
}

export default {
    middleware,
    handler
}