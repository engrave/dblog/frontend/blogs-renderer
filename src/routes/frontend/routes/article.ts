import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { getBlog, getArticle, getFeaturedArticles } from '../../../submodules/shared-library/services/cache/cache';
import { BlogNotExist } from '../../../submodules/shared-library/helpers/errors/BlogNotExist';
import { ArticleNotFound } from '../../../submodules/shared-library/helpers/errors/ArticleNotFound';
import * as httpCodes from 'http-status-codes';

const middleware: any[] =  [];

async function handler(req: Request, res: Response) {

    return handleResponseError(async () => {

        const {hostname} = req;
        const {permlink} = req.params;

        try {

            const blog = await getBlog(hostname);

            if(blog.domain_redirect && blog.custom_domain && hostname != blog.custom_domain) {
                return res.redirect(`https://${blog.custom_domain}/${permlink}`);
            }

            const article = await getArticle(blog._id, permlink);
            const featured = await getFeaturedArticles(blog._id, 0, 10);
        
            return res.render(`${blog.theme}/theme/single.pug`, {
                blog: blog,
                article: article,
                featured: featured,
                cache: true
            });
           
        } catch (error) {
            if(error instanceof BlogNotExist) {
                return res.redirect('https://' + process.env.DOMAIN);
            } else if(error instanceof ArticleNotFound) {            
    
                const blog = await getBlog(hostname);
                const featured = await getFeaturedArticles(blog._id, 0, 10);

                return res.status(httpCodes.NOT_FOUND).render(`${blog.theme}/theme/404.pug`, {
                    blog: blog,
                    featured: featured
                });
            }

            throw error;
        }
        
    }, req, res);
}

export default {
    middleware,
    handler
}